#/bin/bash
rosbag record /odom \
       /mavros/state \
       /mavros/local_position/local \
       /mavros/setpoint_position/local \
       /mavros/vision_pose/pose \
	/flir_image_raw \
	/flir/image/masked \
	/flir/image/tracked \
	/tracking/target_position \
	/camera/rgb/image_raw  \
	/camera/rgb/camera_info \
	/camera/depth_registered/image_raw \
	/ultrasonics_range 


