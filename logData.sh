#/bin/bash
rosbag record /odom \
       /guidance/obstacle_distance \
       /guidance/ultrasonic \
       /guidance/velocity \
       /mavros/state \
       /mavros/local_position/local \
       /mavros/setpoint_position/local \
       /mavros/vision_pose/pose
