#/bin/bash
rosbag record /odom \
       /mavros/state \
       /mavros/local_position/local \
       /mavros/setpoint_position/local \
       /mavros/vision_pose/pose \
	/flir_image_raw \
	/flir/image/masked \
	/flir/image/tracked \
	/tracking/target_position \
	/guidance/left/image_raw  \
	/guidance/left/camera_info \
	/guidance/right/image_raw \
	/guidance/right/camera_info \
	/guidance/ultrasonic


